package cmd

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"

	"easy-book-generator/config"
	"easy-book-generator/utils"

	"github.com/spf13/cobra"
)

// tasksCmd represents the tasks command
var tasksCmd = &cobra.Command{
	Use:   "tasks",
	Short: "List all remaining tasks (todos) or export them to a todo app",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) > 0 && args[0] == "export" {
			if len(args) < 3 {
				fmt.Println("Usage: tasks export <todotxt|microsoft-to-do> <file|list>")
				return
			}
			exportType := args[1]
			target := args[2]
			exportTodos(exportType, target)
		} else {
			listTodos()
		}
	},
}

func init() {
	RootCmd.AddCommand(tasksCmd)
}

// listTodos gathers all tasks from chapters, notes, marketing, expose files, and custom todos from todos.txt
func listTodos() {
	var todos []string

	// Gather custom todos from todos.txt
	todoFilePath := filepath.Join("todos.txt")
	if _, err := os.Stat(todoFilePath); err == nil {
		todos = append(todos, readTodosFromFile(todoFilePath)...)
	}

	// Check chapters, notes, marketing, expose files for todo/draft/review tags
	todos = append(todos, scanForTodos("book", "Chapter")...)
	todos = append(todos, scanForTodos("notes", "Note")...)
	todos = append(todos, scanForTodos("marketing", "Marketing")...)
	todos = append(todos, scanForTodos("expose", "Expose")...)

	// Display todos
	if len(todos) == 0 {
		fmt.Println("No tasks found. Everything seems to be up-to-date!")
	} else {
		fmt.Println("Here are your remaining tasks:")
		for _, todo := range todos {
			fmt.Printf("- %s\n", todo)
		}
	}
}

// exportTodos exports tasks to a specified todo app
func exportTodos(exportType, target string) {
	todos := gatherTodos()
	switch exportType {
	case "todotxt":
		exportToTodoTxt(todos, target)
	case "microsoft-to-do":
		exportToMicrosoftToDo(todos, target)
	default:
		fmt.Println("Unsupported export type. Use 'todotxt' or 'microsoft-to-do'.")
	}
}

// exportToMicrosoftToDo exports tasks to Microsoft To-Do using Microsoft Graph API
func exportToMicrosoftToDo(todos []string, listName string) {
	// Step 1: Retrieve API credentials from config or prompt user
	_, _, _, accessToken := getMicrosoftApiCredentials()

	if accessToken == "" {
		fmt.Println("Error: Access token not found. Please authenticate with Microsoft.")
		return
	}

	// Step 2: Fetch the target list or create it if it doesn't exist
	listID, err := utils.GetMicrosoftToDoListID(accessToken, listName)
	if err != nil {
		fmt.Printf("Error fetching Microsoft To-Do list: %v\n", err)
		return
	}

	// Step 3: Add tasks to the specified Microsoft To-Do list
	for _, task := range todos {
		err := utils.AddTaskToMicrosoftToDoList(accessToken, listID, task)
		if err != nil {
			fmt.Printf("Error adding task to Microsoft To-Do: %v\n", err)
		}
	}

	fmt.Printf("Tasks successfully exported to Microsoft To-Do list '%s'.\n", listName)
}

// getMicrosoftApiCredentials retrieves API credentials from config or prompts the user
func getMicrosoftApiCredentials() (clientID, clientSecret, tenantID, accessToken string) {
	// Try to read the config first
	err := config.LoadConfig() // This will load into the global 'Config' object
	if err != nil {
		fmt.Printf("Error loading configuration: %v\n", err)
	}

	// Retrieve values from the Config object
	clientID = config.Config.MicrosoftClientID
	clientSecret = config.Config.MicrosoftClientSecret
	tenantID = config.Config.MicrosoftTenantID
	accessToken = config.Config.MicrosoftAccessToken

	// Prompt for missing credentials
	if clientID == "" {
		fmt.Print("Enter Microsoft Client ID: ")
		fmt.Scanln(&clientID)
	}
	if clientSecret == "" {
		fmt.Print("Enter Microsoft Client Secret: ")
		fmt.Scanln(&clientSecret)
	}
	if tenantID == "" {
		fmt.Print("Enter Microsoft Tenant ID: ")
		fmt.Scanln(&tenantID)
	}
	
	// If we don't have an access token, authenticate and get one
if accessToken == "" {
	// Attempt to get the access token and handle the error
	var err error
	accessToken, err = utils.GetMicrosoftAccessToken(clientID, clientSecret, tenantID)
	if err != nil {
		fmt.Printf("Error fetching access token: %v\n", err)
		return "", "", "", "" // Return empty values in case of error
	}
	
	// Save credentials, including the access token, to the configuration
	config.SetMicrosoftCredentials(clientID, clientSecret, tenantID, accessToken)
}

	return clientID, clientSecret, tenantID, accessToken
}

// gatherTodos is a helper function to collect todos from the different sources
func gatherTodos() []string {
	var todos []string

	// Gather custom todos from todos.txt
	todoFilePath := filepath.Join("todos.txt")
	if _, err := os.Stat(todoFilePath); err == nil {
		todos = append(todos, readTodosFromFile(todoFilePath)...)
	}

	// Gather todos from chapters, notes, marketing, expose
	todos = append(todos, scanForTodos("book", "Chapter")...)
	todos = append(todos, scanForTodos("notes", "Note")...)
	todos = append(todos, scanForTodos("marketing", "Marketing")...)
	todos = append(todos, scanForTodos("expose", "Expose")...)

	return todos
}

// exportToTodoTxt exports tasks to a todo.txt file, avoiding duplicates and skipping completed tasks
func exportToTodoTxt(todos []string, targetFile string) {
	existingTasks := make(map[string]bool)
	completedTasks := make(map[string]bool)

	// Read existing tasks from the file to avoid duplicates and check completed tasks
	file, err := os.Open(targetFile)
	if err == nil {
		defer file.Close()
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			line := scanner.Text()
			trimmedLine := stripTaskToCore(line)
			if strings.HasPrefix(line, "x ") {
				// Store completed tasks without the 'x ' prefix and date
				completedTasks[trimmedLine] = true
			} else {
				// Store active tasks without the date
				existingTasks[trimmedLine] = true
			}
		}
	} else if !os.IsNotExist(err) {
		fmt.Printf("Error opening %s: %v\n", targetFile, err)
		return
	}

	// Open the file in append mode
	outputFile, err := os.OpenFile(targetFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Printf("Error opening %s: %v\n", targetFile, err)
		return
	}
	defer outputFile.Close()

	writer := bufio.NewWriter(outputFile)
	for _, todo := range todos {
		trimmedTodo := stripTaskToCore(todo)
		// Only append tasks that are not completed and do not already exist
		if !existingTasks[trimmedTodo] && !completedTasks[trimmedTodo] {
			// Write the task to the file
			_, err := fmt.Fprintln(writer, todo)
			if err != nil {
				fmt.Printf("Error writing to %s: %v\n", targetFile, err)
			}
		}
	}
	writer.Flush()
	fmt.Printf("Tasks successfully exported to %s\n", targetFile)
}

// stripTaskToCore removes the date (if any) and completion mark from a task for comparison purposes
func stripTaskToCore(task string) string {
	// Remove the 'x ' for completed tasks
	task = strings.TrimPrefix(task, "x ")
	// Split the task by spaces and ignore the first element if it's a date (YYYY-MM-DD format)
	parts := strings.Fields(task)
	if len(parts) > 0 && isDate(parts[0]) {
		// Return the task without the date
		return strings.Join(parts[1:], " ")
	}
	return task
}

// isDate checks if a string is in the format YYYY-MM-DD
func isDate(s string) bool {
	// Check if the string matches the date format
	if len(s) != 10 {
		return false
	}
	_, err := time.Parse("2006-01-02", s)
	return err == nil
}

// scanForTodos scans directories like notes, marketing, or expose for todos
func scanForTodos(dir, label string) []string {
	var todos []string
	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if filepath.Ext(path) == ".md" {
			tag := getTagFromFile(path)
			if tag != "" {
				todos = append(todos, formatTodoFromTag(tag, fmt.Sprintf("%s: %s", label, strings.TrimPrefix(path, dir))))
			}
		}
		return nil
	})
	if err != nil {
		fmt.Printf("Error reading %s files: %v\n", dir, err)
	}
	return todos
}

// getTagFromFile retrieves the tag (todo, draft, review) from a file
func getTagFromFile(filePath string) string {
	content, err := os.ReadFile(filePath)
	if err != nil {
		return ""
	}
	if strings.Contains(string(content), "tag: todo") {
		return "todo"
	}
	if strings.Contains(string(content), "tag: draft") {
		return "draft"
	}
	if strings.Contains(string(content), "tag: review") {
		return "review"
	}
	return ""
}

// formatTodoFromTag formats a task based on the file tag
func formatTodoFromTag(tag, label string) string {
	switch tag {
	case "todo":
		return fmt.Sprintf("Write %s", label)
	case "draft":
		return fmt.Sprintf("Finalize %s", label)
	case "review":
		return fmt.Sprintf("Review %s", label)
	}
	return ""
}

// readTodosFromFile reads custom todos from the todos.txt file, excluding completed tasks (lines starting with "x ")
func readTodosFromFile(filePath string) []string {
	var todos []string
	file, err := os.Open(filePath)
	if err != nil {
		fmt.Printf("Error reading todos.txt: %v\n", err)
		return todos
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if !strings.HasPrefix(line, "x ") {
			// Strip dates from the task if they exist
			line = stripDateFromTask(line)
			todos = append(todos, line)
		}
	}
	return todos
}

// stripDateFromTask removes the leading date from a task line
func stripDateFromTask(task string) string {
	// Check if the task starts with a date (formatted as YYYY-MM-DD)
	if len(task) > 10 && task[4] == '-' && task[7] == '-' {
		return task[11:] // Remove the date and space
	}
	return task
}