package cmd

import (
	"fmt"
	"path/filepath"
	"strings"

	"easy-book-generator/utils"
	"github.com/spf13/cobra"
)

// PageStatus represents the status of a page
type PageStatus struct {
	Todo   int
	Draft  int
	Review int
	Done   int
}

var statusCmd = &cobra.Command{
	Use:   "status [all|todo|draft|review|done]",
	Short: "Check the status of all pages in the book",
	Run: func(cmd *cobra.Command, args []string) {
		checkStatus(args)
	},
}

func init() {
	RootCmd.AddCommand(statusCmd)
}

func checkStatus(args []string) {
	bookMetadata := utils.ReadBookMetadata()
	status := PageStatus{}
	filesByStatus := make(map[string][]string)

	// Analyze the status of each chapter based on its metadata tag
	if bookMetadata.HasParts {
		// When parts are enabled, loop through each part and its chapters
		for _, part := range bookMetadata.Parts {
			for _, chapter := range part.Chapters {
				chapterPath := filepath.Join("book", utils.Slugify(part.Part), chapter)
				processChapterStatus(chapterPath, chapter, &status, filesByStatus)
			}
		}
	} else {
		// No parts, just loop through the flat list of chapters
		for _, chapter := range bookMetadata.Chapters {
			chapterPath := filepath.Join("book", chapter)
			processChapterStatus(chapterPath, chapter, &status, filesByStatus)
		}
	}

	// Display status summary
	total := status.Todo + status.Draft + status.Review + status.Done
	if total == 0 {
		fmt.Println("No pages found.")
		return
	}

	if len(args) == 0 {
		displayStatusSummary(status, total)
	} else if len(args) == 1 {
		switch args[0] {
		case "all":
			displayFilesByState(filesByStatus)
		case "todo", "draft", "review", "done":
			displayFilesWithState(args[0], filesByStatus[args[0]])
		default:
			fmt.Println("Unknown parameter. Use 'all' or a specific state: todo, draft, review, done.")
		}
	}
}

func processChapterStatus(chapterPath, chapter string, status *PageStatus, filesByStatus map[string][]string) {
	statusLine, err := utils.GetStatusLine(chapterPath)
	if err != nil {
		fmt.Printf("Error getting status for %s: %v\n", chapter, err)
		return
	}
	updateStatus(statusLine, chapterPath, status, filesByStatus)
}

func updateStatus(statusLine, path string, status *PageStatus, filesByStatus map[string][]string) {
	filesByStatus[statusLine] = append(filesByStatus[statusLine], path)

	switch statusLine {
	case "todo":
		status.Todo++
	case "draft":
		status.Draft++
	case "review":
		status.Review++
	case "done":
		status.Done++
	default:
		// No status found or unrecognized status
	}
}

func displayStatusSummary(status PageStatus, total int) {
	fmt.Println("-------------------")
	fmt.Printf("Total TODOs: %d\n", status.Todo)
	fmt.Printf("Total Drafts: %d\n", status.Draft)
	fmt.Printf("Total Reviews: %d\n", status.Review)
	fmt.Printf("Total Dones: %d\n", status.Done)
	fmt.Println("-------------------")

	donePercentage := status.Done * 100 / total
	fmt.Printf("Percentage Done: %d%%\n", donePercentage)
}

func displayFilesByState(filesByStatus map[string][]string) {
	for state, files := range filesByStatus {
		fmt.Printf("%s:\n", strings.Title(state))
		for _, file := range files {
			fmt.Printf("  %s\n", file)
		}
		fmt.Printf("Total %s: %d\n", state, len(files))
	}
}

func displayFilesWithState(state string, files []string) {
	fmt.Printf("%s:\n", strings.Title(state))
	for _, file := range files {
		fmt.Printf("  %s\n", file)
	}
	fmt.Printf("Total %s: %d\n", state, len(files))
}