package cmd

import (
	"easy-book-generator/utils"
	"fmt"
	"path/filepath"

	"github.com/spf13/cobra"
)

// listCmd represents the list command
var listCmd = &cobra.Command{
	Use:   "list",
	Short: "List all parts and chapters in the book",
	Run: func(cmd *cobra.Command, args []string) {
		listPartsAndChapters()
	},
}

func init() {
	RootCmd.AddCommand(listCmd)
}

// listPartsAndChapters lists all parts and chapters from book.yaml
func listPartsAndChapters() {
	bookMetadata := utils.ReadBookMetadata()

	if bookMetadata.HasParts {
		// If parts are enabled, list all parts and their chapters
		fmt.Println("Parts and Chapters:")
		for _, part := range bookMetadata.Parts {
			fmt.Printf("Part: %s\n", part.Part)
			for _, chapter := range part.Chapters {
				fmt.Printf("  Chapter: %s\n", filepath.Base(chapter))
			}
		}
	} else {
		// If parts are not enabled, just list the chapters
		fmt.Println("Chapters:")
		for _, chapter := range bookMetadata.Chapters {
			fmt.Printf("  Chapter: %s\n", filepath.Base(chapter))
		}
	}
}