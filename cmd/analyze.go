package cmd

import (
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/darkliquid/textstats"
	"github.com/spf13/cobra"
)

// Stats holds the analysis metrics for the text
type Stats struct {
	AverageLettersPerWord     float64
	AverageSyllablesPerWord   float64
	AverageWordsPerSentence   float64
	WordCount                 int
	SentenceCount             int
	LetterCount               int
	SyllableCount             int
	FleschKincaidReadingEase  float64
	FleschKincaidGradeLevel   float64
	GunningFogScore           float64
	ColemanLiauIndex          float64
	SMOGIndex                 float64
	AutomatedReadabilityIndex float64
	DaleChallReadabilityScore float64
}

// ScoreInfo defines the readability metrics information
type ScoreInfo struct {
	Name        string
	Explanation string
	Optimum     [2]float64
}

// scoreInfos defines the mapping of scores to their info
var scoreInfos = map[string]ScoreInfo{
	"AverageLettersPerWord": {
		Name:        "Average Letters Per Word",
		Explanation: "Indicates the average number of letters in each word. Ideal range is 4.0 to 5.0.",
		Optimum:     [2]float64{4.0, 5.0},
	},
	"AverageSyllablesPerWord": {
		Name:        "Average Syllables Per Word",
		Explanation: "Indicates the average number of syllables in each word. Ideal range is 1.4 to 1.8.",
		Optimum:     [2]float64{1.4, 1.8},
	},
	"AverageWordsPerSentence": {
		Name:        "Average Words Per Sentence",
		Explanation: "Indicates the average number of words in each sentence. Ideal range is 14 to 20.",
		Optimum:     [2]float64{14, 20},
	},
	"FleschKincaidReadingEase": {
		Name:        "Flesch-Kincaid Reading Ease",
		Explanation: "Indicates how easy a text is to read on a range between 1 and 100. Higher scores indicate easier readability. Ideal range is 60 to 70.",
		Optimum:     [2]float64{60, 70},
	},
	"FleschKincaidGradeLevel": {
		Name:        "Flesch-Kincaid Grade Level",
		Explanation: "Indicates the U.S. school grade level required to understand the text on a scale of 0 to 18. Ideal range is 7 to 8.",
		Optimum:     [2]float64{7, 8},
	},
	"GunningFogScore": {
		Name:        "Gunning Fog Score",
		Explanation: "Generates a grade level between 0 and 20. Estimates the years of formal education needed to understand the text on first reading. Ideal range is 6 to 8.",
		Optimum:     [2]float64{6, 8},
	},
	"ColemanLiauIndex": {
		Name:        "Coleman-Liau Index",
		Explanation: "Indicates the U.S. school grade level required to understand the text. Ideal range is 8 to 10.",
		Optimum:     [2]float64{8, 10},
	},
	"SMOGIndex": {
		Name:        "SMOG Index",
		Explanation: "Estimates the years of education needed to understand the text. Ideal range is 8 to 10.",
		Optimum:     [2]float64{8, 10},
	},
	"AutomatedReadabilityIndex": {
		Name:        "Automated Readability Index",
		Explanation: "Indicates the U.S. school grade level required to understand the text. Ideal range is 8 to 10.",
		Optimum:     [2]float64{8, 10},
	},
	"DaleChallReadabilityScore": {
		Name:        "Dale-Chall Readability Score",
		Explanation: "Estimates the understandability of the text for fourth graders. Ideal range is 6 to 8.",
		Optimum:     [2]float64{6, 8},
	},
}


// analyzeCmd represents the analyze command
var analyzeCmd = &cobra.Command{
	Use:   "analyze [book|part|chapter] [slug] [score]",
	Short: "Analyze the readability and statistics of the book, a part, or a chapter",
	Run: func(cmd *cobra.Command, args []string) {
		Analyze(args)
	},
}

func init() {
	RootCmd.AddCommand(analyzeCmd)
}

// Analyze processes the book, part, or chapter based on the arguments
func Analyze(args []string) {
	if len(args) == 0 {
		analyzeBook("")
		return
	}

	itemType := args[0]
	var slug, specificScore string
	if len(args) > 1 {
		slug = args[1]
	}
	if len(args) > 2 {
		specificScore = args[2]
	}

	switch itemType {
	case "book":
		analyzeBook(specificScore)
	case "part":
		if slug == "" {
			fmt.Println("Usage: ebg analyze part [part-slug] [score]")
			return
		}
		analyzePart(slug, specificScore)
	case "chapter":
		if slug == "" {
			fmt.Println("Usage: ebg analyze chapter [chapter-slug] [score]")
			return
		}
		analyzeChapter(slug, specificScore)
	default:
		fmt.Println("Unknown item type. Please specify 'book', 'part', or 'chapter'.")
	}
}

func analyzeBook(specificScore string) {
	bookDir := "book"
	totalStats := Stats{}
	chapterCount := 0

	err := filepath.Walk(bookDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if !info.IsDir() && filepath.Ext(path) == ".md" {
			stats, err := analyzeFile(path)
			if err != nil {
				return err
			}
			totalStats = addStats(totalStats, stats)
			chapterCount++
		}

		return nil
	})

	if err != nil {
		fmt.Printf("Error analyzing book: %v\n", err)
		return
	}

	averageStats := averageStats(totalStats, chapterCount)
	fmt.Println("Book Analysis:")
	printStats(averageStats, specificScore)
}

func analyzePart(slug string, specificScore string) {
	partDir := filepath.Join("book", slug)
	totalStats := Stats{}
	chapterCount := 0

	err := filepath.Walk(partDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if !info.IsDir() && filepath.Ext(path) == ".md" {
			stats, err := analyzeFile(path)
			if err != nil {
				return err
			}
			totalStats = addStats(totalStats, stats)
			chapterCount++
		}

		return nil
	})

	if err != nil {
		fmt.Printf("Error analyzing part '%s': %v\n", slug, err)
		return
	}

	averageStats := averageStats(totalStats, chapterCount)
	fmt.Printf("Part '%s' Analysis:\n", slug)
	printStats(averageStats, specificScore)
}

func analyzeChapter(slug string, specificScore string) {
	chapterPath := filepath.Join("book", slug+".md")
	stats, err := analyzeFile(chapterPath)
	if err != nil {
		fmt.Printf("Error analyzing chapter ‘%s’: %v\n", slug, err)
		return
	}
	fmt.Printf("Chapter '%s' Analysis:\n", slug)
	printStats(stats, specificScore)
}

func analyzeFile(filePath string) (Stats, error) {
	content, err := os.ReadFile(filePath)
	if err != nil {
		return Stats{}, err
	}

	cleanedContent := cleanContent(string(content))

	statistics := Stats{
		WordCount:                 textstats.WordCount(cleanedContent),
		SentenceCount:             textstats.SentenceCount(cleanedContent),
		LetterCount:               textstats.LetterCount(cleanedContent),
		SyllableCount:             textstats.SyllableCount(cleanedContent),
		AverageLettersPerWord:     textstats.AverageLettersPerWord(cleanedContent),
		AverageSyllablesPerWord:   textstats.AverageSyllablesPerWord(cleanedContent),
		AverageWordsPerSentence:   textstats.AverageWordsPerSentence(cleanedContent),
		FleschKincaidReadingEase:  textstats.FleschKincaidReadingEase(cleanedContent),
		FleschKincaidGradeLevel:   textstats.FleschKincaidGradeLevel(cleanedContent),
		GunningFogScore:           textstats.GunningFogScore(cleanedContent),
		ColemanLiauIndex:          textstats.ColemanLiauIndex(cleanedContent),
		SMOGIndex:                 textstats.SMOGIndex(cleanedContent),
		AutomatedReadabilityIndex: textstats.AutomatedReadabilityIndex(cleanedContent),
		DaleChallReadabilityScore: textstats.DaleChallReadabilityScore(cleanedContent),
	}

	return statistics, nil
}

func cleanContent(content string) string {
	re := regexp.MustCompile(`(?s)^---.*?---`)
	cleanedContent := re.ReplaceAllString(content, "")

	re = regexp.MustCompile(`(?s)~~~mermaid.*?~~~`)
	cleanedContent = re.ReplaceAllString(cleanedContent, "")

	re = regexp.MustCompile("(?s)```.*?```")
	cleanedContent = re.ReplaceAllString(cleanedContent, "")

	return strings.TrimSpace(cleanedContent)
}

func addStats(total, stats Stats) Stats {
	total.WordCount += stats.WordCount
	total.SentenceCount += stats.SentenceCount
	total.LetterCount += stats.LetterCount
	total.SyllableCount += stats.SyllableCount
	total.AverageLettersPerWord += stats.AverageLettersPerWord
	total.AverageSyllablesPerWord += stats.AverageSyllablesPerWord
	total.AverageWordsPerSentence += stats.AverageWordsPerSentence
	total.FleschKincaidReadingEase += stats.FleschKincaidReadingEase
	total.FleschKincaidGradeLevel += stats.FleschKincaidGradeLevel
	total.GunningFogScore += stats.GunningFogScore
	total.ColemanLiauIndex += stats.ColemanLiauIndex
	total.SMOGIndex += stats.SMOGIndex
	total.AutomatedReadabilityIndex += stats.AutomatedReadabilityIndex
	total.DaleChallReadabilityScore += stats.DaleChallReadabilityScore
	return total
}

func averageStats(total Stats, count int) Stats {
	if count == 0 {
		return total
	}
	total.AverageLettersPerWord /= float64(count)
	total.AverageSyllablesPerWord /= float64(count)
	total.AverageWordsPerSentence /= float64(count)
	total.FleschKincaidReadingEase /= float64(count)
	total.FleschKincaidGradeLevel /= float64(count)
	total.GunningFogScore /= float64(count)
	total.ColemanLiauIndex /= float64(count)
	total.SMOGIndex /= float64(count)
	total.AutomatedReadabilityIndex /= float64(count)
	total.DaleChallReadabilityScore /= float64(count)
	return total
}

func printStats(stats Stats, specificScore string) {
	if specificScore == "" {
		fmt.Printf("Word Count: %d\n", stats.WordCount)
		fmt.Printf("Sentence Count: %d\n", stats.SentenceCount)
		fmt.Printf("Letter Count: %d\n", stats.LetterCount)
		fmt.Printf("Syllable Count: %d\n", stats.SyllableCount)
		for key, info := range scoreInfos {
			value := getScoreValue(stats, key)
			fmt.Printf("%s: %s / %.1f\n", info.Name, colorize(value, info.Optimum[0], info.Optimum[1]), info.Optimum[1])
			fmt.Printf("  %s\n", info.Explanation)
		}
	} else {
		info, exists := scoreInfos[specificScore]
		if !exists {
			fmt.Printf("Unknown score: %s\n", specificScore)
			return
		}
		value := getScoreValue(stats, specificScore)
		fmt.Printf("%s: %s / %.1f\n", info.Name, colorize(value, info.Optimum[0], info.Optimum[1]), info.Optimum[1])
		fmt.Printf("  %s\n", info.Explanation)
	}
}

func getScoreValue(stats Stats, key string) float64 {
	switch key {
	case "AverageLettersPerWord":
		return stats.AverageLettersPerWord
	case "AverageSyllablesPerWord":
		return stats.AverageSyllablesPerWord
	case "AverageWordsPerSentence":
		return stats.AverageWordsPerSentence
	case "FleschKincaidReadingEase":
		return stats.FleschKincaidReadingEase
	case "FleschKincaidGradeLevel":
		return stats.FleschKincaidGradeLevel
	case "GunningFogScore":
		return stats.GunningFogScore
	case "ColemanLiauIndex":
		return stats.ColemanLiauIndex
	case "SMOGIndex":
		return stats.SMOGIndex
	case "AutomatedReadabilityIndex":
		return stats.AutomatedReadabilityIndex
	case "DaleChallReadabilityScore":
		return stats.DaleChallReadabilityScore
	default:
		return 0
	}
}

func colorize(value, low, high float64) string {
	if value < low {
		return fmt.Sprintf("\033[31m%.2f\033[0m", value) // Red
	} else if value > high {
		return fmt.Sprintf("\033[31m%.2f\033[0m", value) // Red
	} else {
		return fmt.Sprintf("\033[32m%.2f\033[0m", value) // Green
	}
}
