package cmd

import (
	"easy-book-generator/utils"
	"fmt"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
)

var validateCmd = &cobra.Command{
	Use:   "validate",
	Short: "Check the consistency of the book structure",
	Run: func(cmd *cobra.Command, args []string) {
		validateBookStructure()
	},
}

func init() {
	RootCmd.AddCommand(validateCmd)
}

// validateBookStructure checks the consistency of the book structure using book.yaml
func validateBookStructure() {
	bookDir := "book"
	var missingFiles []string
	var orphanedFiles []string

	// Load the book structure from book.yaml
	bookMetadata := utils.ReadBookMetadata()

	// Map to track all referenced files from book.yaml
	referencedFiles := make(map[string]bool)

	// If parts are enabled, gather references for all chapters within parts
	if bookMetadata.HasParts {
		for _, part := range bookMetadata.Parts {
			for _, chapter := range part.Chapters {
				// Construct the full path to the chapter within the part
				fullPath := filepath.Join(bookDir, utils.Slugify(part.Part), chapter)
				referencedFiles[fullPath] = false
			}
		}
	} else {
		// If no parts, gather references for all chapters in the flat structure
		for _, chapter := range bookMetadata.Chapters {
			fullPath := filepath.Join(bookDir, chapter)
			referencedFiles[fullPath] = false
		}
	}

	// Check if referenced files exist
	for filePath := range referencedFiles {
		if _, err := os.Stat(filePath); os.IsNotExist(err) {
			missingFiles = append(missingFiles, filePath)
		} else {
			referencedFiles[filePath] = true
		}
	}

	// Find all markdown files and check for orphaned files (those not listed in book.yaml)
	err := filepath.Walk(bookDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if filepath.Ext(path) == ".md" {
			relPath, err := filepath.Rel(bookDir, path)
			if err != nil {
				return err
			}

			// Check if the file is not referenced in book.yaml
			if _, found := referencedFiles[filepath.Join(bookDir, relPath)]; !found {
				orphanedFiles = append(orphanedFiles, relPath)
			}
		}

		return nil
	})
	if err != nil {
		fmt.Printf("Error reading markdown files: %v\n", err)
		return
	}

	// Print validation results
	printValidationResults(missingFiles, orphanedFiles)
}

// printValidationResults prints missing and orphaned files
func printValidationResults(missingFiles, orphanedFiles []string) {
	if len(missingFiles) > 0 {
		fmt.Println("Missing files referenced in book.yaml:")
		for _, file := range missingFiles {
			fmt.Printf("  %s\n", file)
		}
	} else {
		fmt.Println("No missing files referenced in book.yaml.")
	}

	if len(orphanedFiles) > 0 {
		fmt.Println("Orphaned files not referenced in book.yaml:")
		for _, file := range orphanedFiles {
			fmt.Printf("  %s\n", file)
		}
	} else {
		fmt.Println("No orphaned files.")
	}
}