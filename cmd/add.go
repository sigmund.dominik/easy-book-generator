package cmd

import (
	"fmt"
	"os"
	"path/filepath"

	"easy-book-generator/utils"
	"github.com/spf13/cobra"
)

// addCmd represents the add command
var addCmd = &cobra.Command{
	Use:   "add [part|chapter|note] <name> [partName|noteType]",
	Short: "Add a new part, chapter, or note to the book",
	Long: `Add a new part, chapter, or note to the book.
- For parts, specify 'part' and the name.
- For chapters, specify 'chapter', the name, and optionally the part name if parts are enabled.
- For notes, specify 'note', the name, and the type of note (character, event, place, item, etc.).`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 2 {
			fmt.Println("Usage: add [part|chapter|note] <name> [partName|noteType]")
			return
		}

		itemType := args[0]
		name := args[1]
		var extraArg string
		if itemType == "chapter" && len(args) > 2 {
			extraArg = args[2]
		} else if itemType == "note" && len(args) > 2 {
			extraArg = args[2] // Note type (e.g., character, event, etc.)
		}

		bookMetadata := utils.ReadBookMetadata()

		switch itemType {
		case "part":
			if bookMetadata.HasParts {
				addPart(name)
			} else {
				fmt.Println("Parts are not enabled in this book. You cannot add a part.")
			}
		case "chapter":
			if bookMetadata.HasParts {
				utils.PromptIfEmpty(&extraArg, "Enter the name of the part:")
				addChapterWithPart(name, extraArg)
			} else {
				addChapterWithoutPart(name)
			}
		case "note":
			if extraArg == "" {
				fmt.Println("Please specify the type of note (e.g., character, event, place, etc.).")
				return
			}
			addNote(name, extraArg)
		default:
			fmt.Println("Unknown item type. Please specify 'part', 'chapter', or 'note'.")
		}
	},
}

func init() {
	RootCmd.AddCommand(addCmd)
}

// addPart adds a new part to the book when parts are enabled
func addPart(name string) {
	slug := utils.Slugify(name)
	bookMetadata := utils.ReadBookMetadata()

	// Add the part to book.yaml
	newPart := utils.Part{
		Part:     name,
		Chapters: []string{},
	}

	// Add the new part to the parts list
	bookMetadata.Parts = append(bookMetadata.Parts, newPart)

	// Save the updated book.yaml
	utils.SaveBookMetadata(bookMetadata)

	// Create a directory for the part
	partDir := filepath.Join("book", slug)
	if err := os.MkdirAll(partDir, 0755); err != nil {
		fmt.Printf("Error creating part directory '%s': %v\n", partDir, err)
		return
	}

	fmt.Printf("Added new part '%s' to the book\n", name)
}

// addChapterWithPart adds a new chapter to a specified part in the book
func addChapterWithPart(name, partName string) {
	bookMetadata := utils.ReadBookMetadata()
	slug := utils.Slugify(name)

	// Find the part in book.yaml
	found := false
	for i, part := range bookMetadata.Parts {
		if part.Part == partName {
			// Add the chapter to the part
			bookMetadata.Parts[i].Chapters = append(bookMetadata.Parts[i].Chapters, slug+".md")
			found = true
			break
		}
	}
	if !found {
		fmt.Printf("Part '%s' does not exist. Please add the part first.\n", partName)
		return
	}

	// Save the updated book.yaml
	utils.SaveBookMetadata(bookMetadata)

	// Create the chapter file
	chapterPath := filepath.Join("book", utils.Slugify(partName), slug+".md")
	utils.CreateChapterFile(chapterPath, name)

	fmt.Printf("Added new chapter '%s' to part '%s'\n", name, partName)
}

// addChapterWithoutPart adds a chapter to the book directly when parts are not enabled
func addChapterWithoutPart(name string) {
	bookMetadata := utils.ReadBookMetadata()
	slug := utils.Slugify(name)

	// Add the chapter directly to the flat list of chapters
	bookMetadata.Chapters = append(bookMetadata.Chapters, slug+".md")

	// Save the updated book.yaml
	utils.SaveBookMetadata(bookMetadata)

	// Create the chapter file in the "book" directory
	chapterPath := filepath.Join("book", slug+".md")
	utils.CreateChapterFile(chapterPath, name)

	fmt.Printf("Added new chapter '%s' to the book\n", name)
}

// addNote adds a note of the specified type to the notes folder
func addNote(name, noteType string) {
	folderMap := map[string]string{
		"character": "characters",
		"event":     "events",
		"place":     "places",
		"item":      "items",
		"theme":     "themes",
		"scene":     "scenes",
		"worldbuilding": "worldbuilding",
	}

	// Get the folder name based on the note type
	noteFolder, ok := folderMap[noteType]
	if !ok {
		fmt.Printf("Unknown note type '%s'. Supported types: character, event, place, item, theme, scene, worldbuilding.\n", noteType)
		return
	}

	// Create the full path for the note
	notePath := filepath.Join("notes", noteFolder, utils.Slugify(name)+".md")

	// Create the note file using the appropriate template
	err := utils.CreateNoteFromTemplate(notePath, noteType+".md", name)
	if err != nil {
		fmt.Printf("Error creating note: %v\n", err)
		return
	}

	fmt.Printf("Added new %s note: %s\n", noteType, name)
}