package cmd

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"easy-book-generator/utils"

	"github.com/spf13/cobra"
)

// renameCmd represents the rename command
var renameCmd = &cobra.Command{
	Use:   "rename <part|chapter> <old-name> <new-name>",
	Short: "Rename a part or chapter in the book",
	Args:  cobra.ExactArgs(3),
	Run: func(cmd *cobra.Command, args []string) {
		itemType := args[0]
		oldName := args[1]
		newName := args[2]

		switch itemType {
		case "part":
			renamePart(oldName, newName)
		case "chapter":
			renameChapter(oldName, newName)
		default:
			fmt.Println("Unknown item type. Please specify 'part' or 'chapter'.")
		}
	},
}

func init() {
	RootCmd.AddCommand(renameCmd)
}

// renamePart renames a part in the book.yaml and updates the directory
func renamePart(oldName, newName string) {
	oldSlug := utils.Slugify(oldName)
	newSlug := utils.Slugify(newName)
	oldPartDir := filepath.Join("book", oldSlug)
	newPartDir := filepath.Join("book", newSlug)

	// Check if old part directory exists
	if _, err := os.Stat(oldPartDir); os.IsNotExist(err) {
		fmt.Printf("Part '%s' does not exist.\n", oldName)
		return
	}

	// Read and update book.yaml
	bookMetadata := utils.ReadBookMetadata()
	found := false
	for i, part := range bookMetadata.Parts {
		if part.Part == oldName {
			bookMetadata.Parts[i].Part = newName
			found = true
			break
		}
	}

	if !found {
		fmt.Printf("Part '%s' not found in book.yaml.\n", oldName)
		return
	}

	// Save the updated book.yaml
	utils.SaveBookMetadata(bookMetadata)

	// Rename the part directory
	if err := os.Rename(oldPartDir, newPartDir); err != nil {
		fmt.Printf("Error renaming part directory from '%s' to '%s': %v\n", oldPartDir, newPartDir, err)
		return
	}

	fmt.Printf("Renamed part '%s' to '%s'\n", oldName, newName)
}

// renameChapter renames a chapter in the book.yaml and updates the file
func renameChapter(oldName, newName string) {
	bookMetadata := utils.ReadBookMetadata()
	newSlug := utils.Slugify(newName)

	// Find if the chapter exists within parts or as a standalone chapter
	found := false
	if bookMetadata.HasParts {
		// The oldName format should be "<part>/<chapter>"
		parts := strings.SplitN(oldName, "/", 2)
		if len(parts) != 2 {
			fmt.Println("Invalid chapter name. Use the format '<part>/<chapter>'")
			return
		}
		partName, chapterName := parts[0], parts[1]
		partSlug := utils.Slugify(partName)

		// Locate part and chapter in book.yaml
		for i, part := range bookMetadata.Parts {
			if part.Part == partName {
				for j, chapter := range part.Chapters {
					if chapter == chapterName+".md" {
						bookMetadata.Parts[i].Chapters[j] = newSlug + ".md"
						found = true
						break
					}
				}
			}
		}
		if !found {
			fmt.Printf("Chapter '%s' not found in part '%s' in book.yaml.\n", chapterName, partName)
			return
		}

		// Save the updated book.yaml
		utils.SaveBookMetadata(bookMetadata)

		// Rename the chapter file
		oldChapterFile := filepath.Join("book", partSlug, utils.Slugify(chapterName)+".md")
		newChapterFile := filepath.Join("book", partSlug, newSlug+".md")
		if err := os.Rename(oldChapterFile, newChapterFile); err != nil {
			fmt.Printf("Error renaming chapter file from '%s' to '%s': %v\n", oldChapterFile, newChapterFile, err)
			return
		}
	} else {
		// No parts, rename the chapter directly
		for i, chapter := range bookMetadata.Chapters {
			if chapter == oldName+".md" {
				bookMetadata.Chapters[i] = newSlug + ".md"
				found = true
				break
			}
		}
		if !found {
			fmt.Printf("Chapter '%s' not found in book.yaml.\n", oldName)
			return
		}

		// Save the updated book.yaml
		utils.SaveBookMetadata(bookMetadata)

		// Rename the chapter file
		oldChapterFile := filepath.Join("book", utils.Slugify(oldName)+".md")
		newChapterFile := filepath.Join("book", newSlug+".md")
		if err := os.Rename(oldChapterFile, newChapterFile); err != nil {
			fmt.Printf("Error renaming chapter file from '%s' to '%s': %v\n", oldChapterFile, newChapterFile, err)
			return
		}
	}

	fmt.Printf("Renamed chapter '%s' to '%s'\n", oldName, newName)
}