package cmd

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"time"

	"easy-book-generator/config"
	"easy-book-generator/templates"
	"easy-book-generator/utils"

	"github.com/spf13/cobra"
)

// Flags for init command
var noGit bool
var noParts bool
var author string
var addNotes bool

// initCmd represents the init command
var initCmd = &cobra.Command{
	Use:   "init [name]",
	Short: "Initialize a new book project",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		name := args[0]

		// If author is not provided via flag, prompt for it
		utils.PromptIfEmpty(&author, "Enter your name (author): ")

		// Save author in config
		config.Config.Author = author
		config.SaveConfig()

		// Slugify the book name
		slug := utils.Slugify(name)
		bookDir := slug

		// Create the necessary directories
		directories := []string{
			bookDir,
			filepath.Join(bookDir, "book"),
			filepath.Join(bookDir, "expose"),
			filepath.Join(bookDir, "export"),
			filepath.Join(bookDir, "marketing"),
			filepath.Join(bookDir, "images"),
		}

		for _, dir := range directories {
			if err := os.MkdirAll(dir, 0755); err != nil {
				fmt.Printf("Error creating directory '%s': %v\n", dir, err)
				return
			}
		}

		// Create the files using templates
		files := []struct {
			subDir   string
			filename string
			tmplName string
			data     interface{}
		}{
			{"", "book.yaml", "book.yaml", struct {
				Title  string
				Slug   string
				Author string
				Parts  bool
				Date   string
			}{
				Title:  name,
				Slug:   slug,
				Author: author,
				Parts:  !noParts,
				Date:   time.Now().Format("2006-01-02"),
			}},
			{"", "README.md", "README.md", struct {
				Title  string
				Author string
			}{
				Title:  name,
				Author: author,
			}},
			{"", "LICENSE.md", "LICENSE.md", struct {
				Year   string
				Author string
			}{
				Year:   time.Now().Format("2006"),
				Author: author,
			}},
			{"", "todos.txt", "todos.txt", nil},
			{"", ".gitignore", "gitignore", nil},
			{"expose", "key_features.md", "key_features.md", nil},
			{"expose", "overview.md", "overview.md", nil},
			{"expose", "selling_points.md", "selling_points.md", nil},
			{"expose", "similar_books.md", "similar_books.md", nil},
			{"expose", "target_audience.md", "target_audience.md", nil},
			{"expose", "about_the_author.md", "about_the_author.md", nil},
			{"marketing", "copy.md", "copy.md", nil},
			{"marketing", "burp.md", "burp.md", nil},
			{"marketing", "categories.md", "categories.md", nil},
			{"marketing", "strategy.md", "strategy.md", nil},
			{"marketing", "social_media.md", "social_media.md", nil},
			{"marketing", "press_kit.md", "press_kit.md", nil},
			{"marketing", "launch_plan.md", "launch_plan.md", nil},
			{"marketing", "email_marketing.md", "email_marketing.md", nil},
			{"marketing", "advertising.md", "advertising.md", nil},
			{"marketing", "influencer_outreach.md", "influencer_outreach.md", nil},
			{"marketing", "reviews.md", "reviews.md", nil},

		}

		for _, file := range files {
			if err := templates.CreateFileFromTemplate(bookDir, file.subDir, file.filename, file.tmplName, file.data); err != nil {
				fmt.Printf("Error creating file '%s': %v\n", file.filename, err)
				return
			}
		}



		if addNotes {
			createNotesFolder(bookDir)
		}

		fmt.Printf("Initialized new book structure in '%s'\n", bookDir)

		// Initialize Git if --no-git flag is not provided
		if !noGit {
			if utils.CheckGit() {
				fmt.Println("Initializing Git repository...")
				cmd := exec.Command("git", "init", bookDir)
				cmd.Run()
			} else {
				fmt.Println("Git is not installed or not available in PATH.")
			}
		}
	},
}

func init() {
	// Add the init command to the root
	RootCmd.AddCommand(initCmd)

	// Define flags for the init command
	initCmd.Flags().BoolVar(&noGit, "no-git", false, "Skip initializing a Git repository")
	initCmd.Flags().BoolVar(&noParts, "no-parts", false, "Initialize the book without parts")
	initCmd.Flags().BoolVar(&addNotes, "add-notes", false, "Create a 'notes' folder with subfolders for organizing notes for fictional works")
	initCmd.Flags().StringVarP(&author, "author", "a", "", "Author of the book")
}
// createNotesFolder creates the 'notes' folder with subfolders for fictional works
func createNotesFolder(bookDir string) {
	notesDir := "notes"
	subfolders := []string{
		"characters",   // For character notes
		"events",       // For event notes
		"places",       // For place notes
		"items",        // For items or artifacts
		"themes",       // For thematic notes or ideas
		"worldbuilding",// For notes related to worldbuilding
		"scenes",       // For scene planning or drafts
	}

	// Create the notes folder and subfolders
	for _, subfolder := range subfolders {
		fullPath := filepath.Join(bookDir, notesDir, subfolder)
		if err := os.MkdirAll(fullPath, 0755); err != nil {
			fmt.Printf("Error creating notes folder '%s': %v\n", fullPath, err)
			return
		}
	}

	fmt.Println("Notes folder and subfolders created.")
}