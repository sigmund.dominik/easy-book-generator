package cmd

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"easy-book-generator/utils"

	"github.com/spf13/cobra"
)

// removeCmd represents the remove command
var removeCmd = &cobra.Command{
	Use:   "remove <part|chapter> <name>",
	Short: "Remove a part or chapter from the book",
	Args:  cobra.ExactArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		itemType := args[0]
		name := args[1]

		switch itemType {
		case "part":
			removePart(name)
		case "chapter":
			removeChapter(name)
		default:
			fmt.Println("Unknown item type. Please specify 'part' or 'chapter'.")
		}
	},
}

func init() {
	RootCmd.AddCommand(removeCmd)
}

// removePart removes a part from the book and updates book.yaml
func removePart(name string) {
	slug := utils.Slugify(name)
	partDir := filepath.Join("book", slug)

	// Check if part directory exists
	if _, err := os.Stat(partDir); os.IsNotExist(err) {
		fmt.Printf("Part '%s' does not exist.\n", name)
		return
	}

	// Load and update book.yaml
	bookMetadata := utils.ReadBookMetadata()
	found := false
	for i, part := range bookMetadata.Parts {
		if part.Part == name {
			bookMetadata.Parts = append(bookMetadata.Parts[:i], bookMetadata.Parts[i+1:]...)
			found = true
			break
		}
	}

	if !found {
		fmt.Printf("Part '%s' not found in book.yaml.\n", name)
		return
	}

	// Save the updated book.yaml
	utils.SaveBookMetadata(bookMetadata)

	// Remove the part directory
	if err := os.RemoveAll(partDir); err != nil {
		fmt.Printf("Error removing part directory '%s': %v\n", partDir, err)
		return
	}

	fmt.Printf("Removed part '%s' from the book\n", name)
}

// removeChapter removes a chapter from a part or directly from the book and updates book.yaml
func removeChapter(name string) {
	bookMetadata := utils.ReadBookMetadata()

	// If parts are enabled, we expect the name to be in "<part>/<chapter>" format
	if bookMetadata.HasParts {
		parts := strings.SplitN(name, "/", 2)
		if len(parts) != 2 {
			fmt.Println("Invalid chapter name. Use the format '<part>/<chapter>'")
			return
		}
		partName, chapterName := parts[0], parts[1]
		partSlug := utils.Slugify(partName)
		chapterSlug := utils.Slugify(chapterName)
		chapterFile := filepath.Join("book", partSlug, chapterSlug+".md")

		// Check if chapter file exists
		if _, err := os.Stat(chapterFile); os.IsNotExist(err) {
			fmt.Printf("Chapter '%s' in part '%s' does not exist.\n", chapterName, partName)
			return
		}

		// Update book.yaml to remove the chapter
		found := false
		for i, part := range bookMetadata.Parts {
			if part.Part == partName {
				for j, chapter := range part.Chapters {
					if chapter == chapterSlug+".md" {
						part.Chapters = append(part.Chapters[:j], part.Chapters[j+1:]...)
						bookMetadata.Parts[i] = part
						found = true
						break
					}
				}
			}
		}

		if !found {
			fmt.Printf("Chapter '%s' not found in part '%s' in book.yaml.\n", chapterName, partName)
			return
		}

		// Save the updated book.yaml
		utils.SaveBookMetadata(bookMetadata)

		// Remove the chapter file
		if err := os.Remove(chapterFile); err != nil {
			fmt.Printf("Error removing chapter file '%s': %v\n", chapterFile, err)
			return
		}

		fmt.Printf("Removed chapter '%s' from part '%s'\n", chapterName, partName)
	} else {
		// No parts, remove the chapter directly
		slug := utils.Slugify(name)
		chapterFile := filepath.Join("book", slug+".md")

		// Check if chapter file exists
		if _, err := os.Stat(chapterFile); os.IsNotExist(err) {
			fmt.Printf("Chapter '%s' does not exist.\n", name)
			return
		}

		// Update book.yaml to remove the chapter
		found := false
		for i, chapter := range bookMetadata.Chapters {
			if chapter == slug+".md" {
				bookMetadata.Chapters = append(bookMetadata.Chapters[:i], bookMetadata.Chapters[i+1:]...)
				found = true
				break
			}
		}

		if !found {
			fmt.Printf("Chapter '%s' not found in book.yaml.\n", name)
			return
		}

		// Save the updated book.yaml
		utils.SaveBookMetadata(bookMetadata)

		// Remove the chapter file
		if err := os.Remove(chapterFile); err != nil {
			fmt.Printf("Error removing chapter file '%s': %v\n", chapterFile, err)
			return
		}

		fmt.Printf("Removed chapter '%s' from the book\n", name)
	}
}