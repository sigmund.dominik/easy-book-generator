package cmd

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"easy-book-generator/utils"
	"github.com/spf13/cobra"
)

// buildCmd represents the build command
var buildCmd = &cobra.Command{
	Use:   "build [book|expose|marketing] [format]",
	Short: "Build the book or expose",
	Run: func(cmd *cobra.Command, args []string) {
		Build(args)
	},
}

func init() {
	RootCmd.AddCommand(buildCmd)
}

// Build handles building the book or expose
func Build(args []string) {
	if len(args) < 1 {
		fmt.Println("Usage: build [book|expose|marketing] [format]")
		return
	}

	if !utils.CheckPandoc() {
		fmt.Println("Error: Pandoc is not installed or not available in PATH.")
		return
	}

	itemType := args[0]
	format := "pdf" // default format
	if len(args) > 1 {
		format = args[1]
	}

	switch itemType {
	case "book":
		buildBook(format)
	case "expose":
		buildExpose(format)
	case "marketing":
		buildMarketing(format)
	default:
		fmt.Println("Unknown item type. Please specify 'book', 'expose' or 'marketing'.")
	}
}

// buildBook builds the book in the specified format
func buildBook(format string) {
	tempDir := filepath.Join(os.TempDir(), "book_build")
	if err := os.MkdirAll(tempDir, 0755); err != nil {
		fmt.Printf("Error creating temporary directory '%s': %v\n", tempDir, err)
		return
	}
	defer os.RemoveAll(tempDir)

	bookTmp := filepath.Join(tempDir, "book.md")

	// Copy all the files to the temp directory
	if err := utils.CopyDir("book", tempDir); err != nil {
		fmt.Printf("Error copying book files: %v\n", err)
		return
	}

	// Process LICENSE.md
	licensePath := filepath.Join(tempDir, "LICENSE.md")
	if err := utils.ProcessLicenseFile("LICENSE.md", licensePath); err != nil {
		fmt.Printf("Error processing LICENSE.md: %v\n", err)
		return
	}

	// Combine the markdown files listed in book.yaml
	metadata := utils.ReadBookMetadata()
	if err := combineMarkdownFilesFromMetadata(metadata, tempDir, bookTmp, format); err != nil {
		fmt.Printf("Error combining markdown files: %v\n", err)
		return
	}

	// Remove tags and fix mermaid fences
	if err := utils.CleanMarkdownFile(bookTmp); err != nil {
		fmt.Printf("Error cleaning markdown file: %v\n", err)
		return
	}

	outputFile := filepath.Join("export", fmt.Sprintf("%s_%s.%s", metadata.Slug, metadata.Version, format))
	if err := utils.ConvertToFormat(bookTmp, "book.yaml", licensePath, outputFile, format); err != nil {
		fmt.Printf("Error converting to %s: %v\n", format, err)
		return
	}

	// Add cover to the PDF if format is PDF
	if format == "pdf" {
		if err := utils.AddPdfCover(metadata.CoverImage, outputFile); err != nil {
			fmt.Printf("Error adding PDF cover: %v\n", err)
			return
		}
	}

	fmt.Printf("Book built successfully: %s\n", outputFile)
	openFile(outputFile)
}

// combineMarkdownFilesFromMetadata combines the markdown files listed in the book.yaml into a single file
func combineMarkdownFilesFromMetadata(metadata *utils.BookMetadata, tempDir, outputFile, format string) error {
	var combinedContent strings.Builder

	// If parts are enabled, loop through parts and their chapters
	if metadata.HasParts {
		for _, part := range metadata.Parts {
			// Add the part name as a top-level header
			combinedContent.WriteString(fmt.Sprintf("# %s\n\n", part.Part))

			// Loop through each chapter in the part
			for _, chapter := range part.Chapters {
				chapterPath := filepath.Join(tempDir, utils.Slugify(part.Part), chapter)
				content, err := os.ReadFile(chapterPath)
				if err != nil {
					return fmt.Errorf("error reading chapter '%s': %v", chapter, err)
				}

				// Adjust chapter headings by shifting them down by one level (e.g., # to ##)
				adjustedContent := shiftMarkdownHeadings(string(content), 1)

				// Append the adjusted content
				combinedContent.WriteString(adjustedContent + "\n\n")

				// Add a page break based on the format
				addPageBreak(&combinedContent, format)
			}
		}
	} else {
		// No parts, just combine the chapters
		for _, chapter := range metadata.Chapters {
			chapterPath := filepath.Join(tempDir, chapter)
			content, err := os.ReadFile(chapterPath)
			if err != nil {
				return fmt.Errorf("error reading chapter '%s': %v", chapter, err)
			}
			combinedContent.WriteString(string(content) + "\n\n")

			// Add a page break based on the format
			addPageBreak(&combinedContent, format)
		}
	}

	// Write the combined content to the output file
	return os.WriteFile(outputFile, []byte(combinedContent.String()), 0644)
}

// addPageBreak adds a page break based on the specified format
func addPageBreak(combinedContent *strings.Builder, format string) {
	// Add a page break based on format (PDF uses \newpage, others use HTML/CSS)
	if format == "pdf" {
		combinedContent.WriteString("\\newpage\n\n")
	} else if format == "epub" || format == "html" {
		combinedContent.WriteString("<div style=\"page-break-before: always;\"></div>\n\n")
	}
}

// shiftMarkdownHeadings shifts the level of markdown headers by the given amount.
// For example, shifting by 1 converts # to ##, ## to ###, and so on.
func shiftMarkdownHeadings(content string, shiftAmount int) string {
	lines := strings.Split(content, "\n")
	var adjustedLines []string

	for _, line := range lines {
		// Check if the line is a markdown heading (starts with #)
		if strings.HasPrefix(line, "#") {
			// Count the number of leading #
			headerLevel := 0
			for i := 0; i < len(line) && line[i] == '#'; i++ {
				headerLevel++
			}

			// Shift the heading level by shiftAmount, but ensure it doesn't exceed the maximum level of 6
			newHeaderLevel := headerLevel + shiftAmount
			if newHeaderLevel > 6 {
				newHeaderLevel = 6
			}

			// Create the adjusted heading with the new level
			adjustedLine := strings.Repeat("#", newHeaderLevel) + line[headerLevel:]
			adjustedLines = append(adjustedLines, adjustedLine)
		} else {
			// Non-header lines remain unchanged
			adjustedLines = append(adjustedLines, line)
		}
	}

	return strings.Join(adjustedLines, "\n")
}

// buildExpose builds the expose in the specified format (pdf, epub, html)
func buildExpose(format string) {
	tempDir := filepath.Join(os.TempDir(), "expose_build")
	if err := os.MkdirAll(tempDir, 0755); err != nil {
		fmt.Printf("Error creating temporary directory '%s': %v\n", tempDir, err)
		return
	}
	defer os.RemoveAll(tempDir)

	exposeDir := "expose"

	// expose files
	exposeFiles := []string{
		"overview.md",
		"about_the_author.md",
		"key_features.md",
		"target_audience.md",
		"similar_books.md",
		"selling_points.md",
	}

	// Copy the files to the temp directory
	for _, file := range exposeFiles {
		srcFile := filepath.Join(exposeDir, file)
		dstFile := filepath.Join(tempDir, file)

		if _, err := os.Stat(srcFile); err == nil {
			// File exists, copy it
			err := utils.CopyFile(srcFile, dstFile)
			if err != nil {
				fmt.Printf("Error copying file '%s': %v\n", file, err)
			} else {
				fmt.Printf("Added expose file: %s\n", file)
			}
		} else if os.IsNotExist(err) {
			// File does not exist, skip it
			fmt.Printf("Skipping missing expose file: %s\n", file)
		}
	}
	// Add the text sample if available in book.yaml
	bookMetadata := utils.ReadBookMetadata() // Assuming this reads the book.yaml metadata

	if bookMetadata.TextSample != "" {
			textSamplePath := filepath.Join("book", bookMetadata.TextSample)
			dstTextSample := filepath.Join(tempDir, "text_sample.md")

			if _, err := os.Stat(textSamplePath); err == nil {
					// File exists, copy it
					err := utils.CopyFile(textSamplePath, dstTextSample)
					if err != nil {
							fmt.Printf("Error copying text sample file: %v\n", err)
					} else {
							fmt.Printf("Added text sample from chapter: %s\n", bookMetadata.TextSample)
					}
			} else {
					fmt.Printf("Skipping missing text sample chapter: %s\n", bookMetadata.TextSample)
			}
	}

	// Process and build combined expose file
	exposeTmp := filepath.Join(tempDir, "expose.md")
	if err := utils.CombineMarkdownFiles(filepath.Join(tempDir), exposeTmp, format); err != nil {
		fmt.Printf("Error combining expose markdown files: %v\n", err)
		return
	}

	// Process LICENSE.md
	licensePath := filepath.Join(tempDir, "LICENSE.md")
	if err := utils.ProcessLicenseFile("LICENSE.md", licensePath); err != nil {
		fmt.Printf("Error processing LICENSE.md: %v\n", err)
		return
	}

		

	// Add "Expose" to the title in metadata
	metadataTmp := filepath.Join(tempDir, "book.yaml")
	// Copy book.yaml to the temp directory

	if err := utils.CopyFile("book.yaml", metadataTmp); err != nil {
		fmt.Printf("Error copying book.yaml: %v\n", err)
		return
	}
	if err := utils.UpdateTitleInMetadata(metadataTmp, "Expose"); err != nil {
		fmt.Printf("Error updating metadata title: %v\n", err)
		return
	}

	// Convert to the desired format (e.g., pdf, epub, html)
	outputFile := filepath.Join("export", fmt.Sprintf("expose.%s", format))
	if err := utils.ConvertToFormat(exposeTmp, metadataTmp, licensePath, outputFile, format); err != nil {
		fmt.Printf("Error converting expose to %s: %v\n", format, err)
		return
	}

	fmt.Printf("Expose built successfully: %s\n", outputFile)
	openFile(outputFile)
}

// buildMarketing builds the marketing materials, adding any extra files and skipping missing ones.
func buildMarketing(format string) {
	marketingDir := "marketing"
	tempDir := filepath.Join(os.TempDir(), "marketing_build")
	if err := os.MkdirAll(tempDir, 0755); err != nil {
		fmt.Printf("Error creating temporary directory '%s': %v\n", tempDir, err)
		return
	}
	defer os.RemoveAll(tempDir)

	// Default marketing files
	marketingFiles := []string{
		"copy.md",
		"burp.md",
		"categories.md",
		"strategy.md",
		"social_media.md",
		"press_kit.md",
		"launch_plan.md",
		"email_marketing.md",
		"advertising.md",
		"influencer_outreach.md",
		"reviews.md",
	}

	// Append any additional files from the marketing directory
	additionalFiles, err := getAdditionalMarketingFiles(marketingDir, marketingFiles)
	if err != nil {
		fmt.Printf("Error getting additional marketing files: %v\n", err)
		return
	}
	marketingFiles = append(marketingFiles, additionalFiles...)

	// Copy the files to the temp directory
	for _, file := range marketingFiles {
		srcFile := filepath.Join(marketingDir, file)
		dstFile := filepath.Join(tempDir, file)

		if _, err := os.Stat(srcFile); err == nil {
			// File exists, copy it
			err := utils.CopyFile(srcFile, dstFile)
			if err != nil {
				fmt.Printf("Error copying file '%s': %v\n", file, err)
			} else {
				fmt.Printf("Added marketing file: %s\n", file)
			}
		} else if os.IsNotExist(err) {
			// File does not exist, skip it
			fmt.Printf("Skipping missing marketing file: %s\n", file)
		}
	}

	// Process and build combined marketing file
	marketingTmp := filepath.Join(tempDir, "marketing.md")
	err = utils.CombineMarkdownFiles(filepath.Join(tempDir), marketingTmp, format)
	if err != nil {
		fmt.Printf("Error combining marketing markdown files: %v\n", err)
		return
	}

	// Add Marketing Strategy to the title in metadata
	metadataTmp := filepath.Join(tempDir, "book.yaml")

	// Copy book.yaml to the temp directory
	err = utils.CopyFile("book.yaml", metadataTmp)
	if err != nil {
		fmt.Printf("Error copying book.yaml: %v\n", err)
		return
	}

	// Update the title to include "Marketing"
	if err := utils.UpdateTitleInMetadata(metadataTmp, "Marketing"); err != nil {
		fmt.Printf("Error updating metadata title: %v\n", err)
		return
	}

	// Save the marketing file to the export directory
	outputFile := filepath.Join("export", "marketing." + format)
	if err := utils.ConvertToFormat(marketingTmp, metadataTmp, "", outputFile, format); err != nil {
		fmt.Printf("Error converting marketing files to %s: %v\n", format, err)
		return
	}

	fmt.Printf("Marketing materials built successfully: %s\n", outputFile)
	openFile(outputFile)
}

// getAdditionalMarketingFiles retrieves additional files from the marketing directory that are not in the default list
func getAdditionalMarketingFiles(marketingDir string, defaultFiles []string) ([]string, error) {
	var additionalFiles []string
	err := filepath.Walk(marketingDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() && filepath.Ext(path) == ".md" {
			fileName := filepath.Base(path)
			// Check if the file is already in the default list
			if !contains(defaultFiles, fileName) {
				additionalFiles = append(additionalFiles, fileName)
			}
		}
		return nil
	})
	return additionalFiles, err
}

// contains checks if a slice contains a specific string
func contains(slice []string, item string) bool {
	for _, elem := range slice {
		if elem == item {
			return true
		}
	}
	return false
}

// openFile tries to open the file in the default application
func openFile(filePath string) {
	cmd := exec.Command("open", filePath)
	cmd.Start()
}