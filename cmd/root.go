package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var RootCmd = &cobra.Command{
	Use:   "ebg",
	Short: "Easy Book Generator",
	Long:  `Easy Book Generator (ebg) is a tool for generating books from markdown files.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Easy Book Generator (ebg) tool. Use `ebg help` for more information.")
	},
}

// Execute is the entry point for the Cobra application
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Add global flags if needed
	// RootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.ebg-config)")
}

// initConfig initializes the configuration using Viper
func initConfig() {
	viper.SetConfigName(".ebg-config")
	viper.AddConfigPath("$HOME")
	viper.AutomaticEnv() // Read in environment variables that match

	// If a config file is found, read it in
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}