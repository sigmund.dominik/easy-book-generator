# Use an official Golang image as a build stage
FROM golang:1.22 AS builder

# Set the Current Working Directory inside the container
WORKDIR /app

# Copy the Go Modules manifests
COPY go.mod ./
COPY go.sum ./

# Download all dependencies. Dependencies will be cached if the go.mod and go.sum files are not changed
# RUN go mod download

# Copy the source from the current directory to the Working Directory inside the container
COPY . .

# Build the Go app for Linux
RUN GOOS=linux GOARCH=amd64 go build -o ebg

# Use an official Debian image as the base image
FROM debian:latest

# Install dependencies
RUN apt-get update && apt-get install -y \
    git \
    pandoc \
    curl \
    gpg \
    unzip \
    imagemagick \
    poppler-utils \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /etc/apt/keyrings;
RUN curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg;
RUN echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_20.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list;
RUN apt-get update && apt-get install -y nodejs 
RUN npm install --global mermaid-filter

# Set the Current Working Directory inside the container
WORKDIR /app

# Copy the built binary from the builder stage
COPY --from=builder /app/ebg /usr/local/bin/ebg

# Default command
CMD ["ebg"]