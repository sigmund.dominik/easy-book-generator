package utils

import (
	"bufio"
	"bytes"
	"easy-book-generator/templates"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"gopkg.in/yaml.v2"
)

type Part struct {
	Part     string   `yaml:"part,omitempty"` // Used only when parts are enabled
	Chapters []string `yaml:"chapters"`
}

type BookMetadata struct {
	Title          string        `yaml:"title"`
	Subtitle       string        `yaml:"subtitle"`
	Keywords       []string      `yaml:"keywords"`
	Abstract       string        `yaml:"abstract"`
	Slug           string        `yaml:"title_slug"`
	CoverImage     string        `yaml:"cover-image"`
	Author         string        `yaml:"author"`
	Date           string        `yaml:"date"`
	Version        string        `yaml:"version"`
	Lang           string        `yaml:"lang"`
	TOC            bool          `yaml:"toc"`
	TOCDepth       int           `yaml:"toc-depth"`
	TOCTitle       string        `yaml:"toc-title"`
	NumberSections bool          `yaml:"numbersections"`
	IncludeBefore  []string      `yaml:"include-before"`
	HasParts       bool          `yaml:"has-parts"` // True if the book uses parts
	TextSample		 string        `yaml:"text-sample"` // Text sample for the expose (one chapter)
	Chapters       []string      `yaml:"chapters,omitempty"` // Flat array for chapters if no parts
	Parts          []Part `yaml:"parts,omitempty"`   // Array of parts, each with its own chapters
}

// CheckGit checks if Git is installed and available in PATH
func CheckGit() bool {
	_, err := exec.LookPath("git")
	return err == nil
}

// CheckPandoc checks if Pandoc is installed and available in PATH
func CheckPandoc() bool {
	_, err := exec.LookPath("pandoc")
	return err == nil
}

// CheckImageMagick checks if ImageMagick is installed and available in PATH (for the cover)
func CheckImageMagick() bool {
	_, err := exec.LookPath("convert")
	return err == nil
}

// CheckPdfunite checks if pdfunite is installed and available in PATH (for combining PDFs)
func CheckPdfunite() bool {
	_, err := exec.LookPath("pdfunite")
	return err == nil
}

// Slugify converts a string into a slug suitable for filenames and URLs
func Slugify(name string) string {
	re := regexp.MustCompile("[^a-z0-9]+")
	slug := re.ReplaceAllString(strings.ToLower(name), "-")
	return strings.Trim(slug, "-")
}

// PromptIfEmpty prompts the user for input if the given variable is empty
func PromptIfEmpty(value *string, prompt string) {
	if *value == "" {
		reader := bufio.NewReader(os.Stdin)
		fmt.Print(prompt)
		input, _ := reader.ReadString('\n')
		*value = strings.TrimSpace(input)
	}
}

// CopyDir copies a directory recursively
func CopyDir(src, dest string) error {
	return filepath.Walk(src, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		relPath, err := filepath.Rel(src, path)
		if err != nil {
			return err
		}

		destPath := filepath.Join(dest, relPath)

		if info.IsDir() {
			return os.MkdirAll(destPath, info.Mode())
		}

		return CopyFile(path, destPath)
	})
}

// copyFile copies a file from src to dest
func CopyFile(src, dest string) error {
	sourceFile, err := os.Open(src)
	if err != nil {
		return err
	}
	defer sourceFile.Close()

	destFile, err := os.Create(dest)
	if err != nil {
		return err
	}
	defer destFile.Close()

	_, err = io.Copy(destFile, sourceFile)
	return err
}

// UpdateMarkdownHeaders updates markdown headers to increase their level
func UpdateMarkdownHeaders(dir string) error {
	return filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() || filepath.Base(path) == "_toc.md" {
			return nil
		}

		content, err := os.ReadFile(path)
		if err != nil {
			return err
		}

		updatedContent := strings.ReplaceAll(string(content), "#", "##")
		return os.WriteFile(path, []byte(updatedContent), info.Mode())
	})
}

// UpdateIncludePaths updates include paths to be relative to the temp directory
func UpdateIncludePaths(dir, prefix string) error {
	return filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() || filepath.Base(path) != "_toc.md" {
			return nil
		}

		content, err := os.ReadFile(path)
		if err != nil {
			return err
		}

		updatedContent := strings.ReplaceAll(string(content), "!include book", fmt.Sprintf("!include %s", dir))
		return os.WriteFile(path, []byte(updatedContent), info.Mode())
	})
}

// CombineMarkdownFiles combines all markdown files from a directory into a single file
func CombineMarkdownFiles(inputDir, outputPath string, format string) error {
	var combinedContent strings.Builder

	// Iterate through all markdown files in the input directory
	err := filepath.Walk(inputDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		// Only process .md files
		if filepath.Ext(path) == ".md" {
			content, err := os.ReadFile(path)
			if err != nil {
				return fmt.Errorf("error reading markdown file '%s': %v", path, err)
			}

			combinedContent.WriteString(string(content) + "\n\n")

			// Add a page break based on format (PDF uses \newpage, others use HTML/CSS)
			if format == "pdf" {
				combinedContent.WriteString("\\newpage\n\n")
			} else if format == "epub" || format == "html" {
				combinedContent.WriteString("<div style=\"page-break-before: always;\"></div>\n\n")
			}
		}
		return nil
	})

	if err != nil {
		return fmt.Errorf("error processing markdown files in directory '%s': %v", inputDir, err)
	}

	// Write the combined content to the output file
	return os.WriteFile(outputPath, []byte(combinedContent.String()), 0644)
}

// CleanMarkdownFile removes tags and fixes mermaid fences in a markdown file
func CleanMarkdownFile(filePath string) error {
	content, err := os.ReadFile(filePath)
	if err != nil {
		return err
	}

	cleanedContent := strings.ReplaceAll(string(content), "---", "")
	cleanedContent = regexp.MustCompile(`(?m)^tag:.*\n?`).ReplaceAllString(cleanedContent, "")
	cleanedContent = regexp.MustCompile(`(?m)^description:.*\n?`).ReplaceAllString(cleanedContent, "")
	cleanedContent = regexp.MustCompile(`(?m)^created:.*\n?`).ReplaceAllString(cleanedContent, "")
	cleanedContent = regexp.MustCompile(`(?m)^author:.*\n?`).ReplaceAllString(cleanedContent, "")
	cleanedContent = strings.ReplaceAll(cleanedContent, "```mermaid", "~~~mermaid")
	cleanedContent = strings.ReplaceAll(cleanedContent, "```", "~~~")
	cleanedContent = strings.ReplaceAll(cleanedContent, "\n\n\n", "\n\n")

	return os.WriteFile(filePath, []byte(cleanedContent), 0644)
}

// ProcessLicenseFile processes the LICENSE.md file, replaces the title, and adds a new page
func ProcessLicenseFile(inputPath, outputPath string) error {
	content, err := os.ReadFile(inputPath)
	if err != nil {
		return err
	}

	// Replace the title
	re := regexp.MustCompile(`^# MIT License`)
	modifiedContent := re.ReplaceAllString(string(content), `\newpage{}\section*{Copyright}`)

	// Add a new page at the end of the file
	modifiedContent += "\n\\newpage{}\n"

	return os.WriteFile(outputPath, []byte(modifiedContent), 0644)
}

// ConvertToFormat converts a markdown file to the specified format using Pandoc
func ConvertToFormat(inputPath, metadataPath, licensePath, outputPath, format string) error {
	// Print out file paths for debugging
	fmt.Printf("Converting file: %s\n", inputPath)
	if metadataPath != "" {
		fmt.Printf("Using metadata file: %s\n", metadataPath)
		err := checkAndRemoveCoverImage(metadataPath)
        if err != nil {
            return fmt.Errorf("error checking/removing cover image: %v", err)
        }
	}
	if licensePath != "" {
		fmt.Printf("Using license file: %s\n", licensePath)
	}

	// Check if the input file exists
	if _, err := os.Stat(inputPath); os.IsNotExist(err) {
		return fmt.Errorf("input file does not exist: %s", inputPath)
	}

	// Check if the metadata file exists (if provided)
	if metadataPath != "" {
		if _, err := os.Stat(metadataPath); os.IsNotExist(err) {
			return fmt.Errorf("metadata file does not exist: %s", metadataPath)
		}
	}

	// Check if the license file exists (if provided)
	if licensePath != "" {
		if _, err := os.Stat(licensePath); os.IsNotExist(err) {
			return fmt.Errorf("license file does not exist: %s", licensePath)
		}
	}

	args := []string{inputPath, "-F", "mermaid-filter", "--toc"}

	// Only add metadata file if it's provided
	if metadataPath != "" {
		args = append(args, "--metadata-file="+metadataPath)
	}

	if format == "html" {
		zipOutputPath := outputPath[:len(outputPath)-len(filepath.Ext(outputPath))] + ".zip"
		args = append(args, "-t", "chunkedhtml")
		args = append(args, "--split-level=2")
		args = append(args, "-o", zipOutputPath)
	} else if format == "epub" {
		args = append(args, "--split-level=2")
		args = append(args, "--number-sections")
		args = append(args, "-o", outputPath)
	} else {
		args = append(args, "--split-level=2")
		args = append(args, "-o", outputPath)
		if licensePath != "" {
			args = append(args, "--include-before-body="+licensePath)
		}
	}

	cmd := exec.Command("pandoc", args...)
	// Create a buffer to capture the stderr output
	var stderr bytes.Buffer
	cmd.Stderr = &stderr

	// Run the command and capture any errors
	err := cmd.Run()
	if err != nil {
		return fmt.Errorf("pandoc conversion failed: %v, stderr: %s", err, stderr.String())
	}

	return nil
}
func checkAndRemoveCoverImage(metadataPath string) error {
	// Step 1: Read the metadata YAML file
	content, err := os.ReadFile(metadataPath)
	if err != nil {
			return fmt.Errorf("error reading metadata file: %v", err)
	}

	lines := strings.Split(string(content), "\n")
	updatedLines := make([]string, 0, len(lines))

	// Step 2: Check if the cover image exists
	coverImagePath := ""
	for _, line := range lines {
			if strings.HasPrefix(line, "cover-image:") {
					// Extract the cover image path
					coverImagePath = strings.TrimSpace(strings.TrimPrefix(line, "cover-image:"))
					coverImagePath = strings.Trim(coverImagePath, `"'`)
			}
			updatedLines = append(updatedLines, line)
	}

	// Step 3: If the cover image is specified but doesn't exist, remove the line
	if coverImagePath != "" {
			if _, err := os.Stat(coverImagePath); os.IsNotExist(err) {
					// Remove the cover-image line if the file does not exist
					updatedLines = removeCoverImageLine(updatedLines)
			}
	}

	// Step 4: Write the updated metadata back to the temporary metadata file
	return os.WriteFile(metadataPath, []byte(strings.Join(updatedLines, "\n")), 0644)
}

// Helper function to remove the 'cover-image' line from the metadata
func removeCoverImageLine(lines []string) []string {
	updatedLines := make([]string, 0, len(lines))
	for _, line := range lines {
			if strings.HasPrefix(line, "cover-image:") {
					continue // Skip the cover-image line
			}
			updatedLines = append(updatedLines, line)
	}
	return updatedLines
}
// AddPdfCover adds a cover image to a PDF using ImageMagick and pdfunite
func AddPdfCover(coverPath, pdfPath string) error {
	tempDir := filepath.Dir(pdfPath)
	contentPdf := filepath.Join(tempDir, "book_content.pdf")
	if err := os.Rename(pdfPath, contentPdf); err != nil {
		return err
	}

	coverPdf := filepath.Join(tempDir, "cover.pdf")
	cmd := exec.Command("convert", coverPath, coverPdf)
	if err := cmd.Run(); err != nil {
		return err
	}

	cmd = exec.Command("pdfunite", coverPdf, contentPdf, pdfPath)
	return cmd.Run()
}

// UpdateTitleInMetadata appends a suffix to the title in the metadataTmp file
func UpdateTitleInMetadata(metadataTmp, suffix string) error {
	// Read the metadata file
	content, err := os.ReadFile(metadataTmp)
	if err != nil {
		return fmt.Errorf("error reading metadata file: %v", err)
	}

	// Convert the file content to a string
	metadata := string(content)

	// Find the existing title and append the suffix
	startIndex := strings.Index(metadata, "title: ")
	if startIndex == -1 {
		return fmt.Errorf("title field not found in metadata")
	}

	// Extract the current title
	startIndex += len("title: ")
	endIndex := strings.Index(metadata[startIndex:], "\n")
	if endIndex == -1 {
		endIndex = len(metadata) - startIndex
	}

	// Current title
	currentTitle := strings.TrimSpace(metadata[startIndex : startIndex+endIndex])

	// Append the suffix to the title
	newTitle := currentTitle + " - " + suffix

	// Replace the old title with the new title
	updatedMetadata := strings.Replace(metadata, "title: "+currentTitle, "title: "+newTitle, 1)

	// Write the updated content back to the file
	if err := os.WriteFile(metadataTmp, []byte(updatedMetadata), 0644); err != nil {
		return fmt.Errorf("error writing updated metadata: %v", err)
	}

	return nil
}


// ReplaceIncludeInFile replaces an include statement in a file
func ReplaceIncludeInFile(filePath, oldInclude, newInclude string) error {
	content, err := os.ReadFile(filePath)
	if err != nil {
		return err
	}
	newContent := strings.ReplaceAll(string(content), oldInclude, newInclude)
	return os.WriteFile(filePath, []byte(newContent), 0644)
}

// GetStatusLine reads the status line (tag) from a markdown file
func GetStatusLine(filePath string) (string, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return "", err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "tag:") {
			return strings.TrimSpace(strings.TrimPrefix(line, "tag:")), nil
		}
	}

	return "", scanner.Err()
}

// readBookMetadata reads the book.yaml configuration file
func ReadBookMetadata() *BookMetadata {
	var metadata BookMetadata
	content, err := os.ReadFile("book.yaml")
	if err != nil {
		fmt.Printf("Error reading book.yaml: %v\n", err)
		return &metadata
	}

	err = yaml.Unmarshal(content, &metadata)
	if err != nil {
		fmt.Printf("Error parsing book.yaml: %v\n", err)
	}

	return &metadata
}

// saveBookMetadata saves the updated book.yaml configuration
func SaveBookMetadata(metadata *BookMetadata) {
	out, err := yaml.Marshal(metadata)
	if err != nil {
		fmt.Printf("Error serializing book metadata: %v\n", err)
		return
	}
	err = os.WriteFile("book.yaml", out, 0644)
	if err != nil {
		fmt.Printf("Error writing book.yaml: %v\n", err)
	}
}

// CreateChapterFile creates a new chapter file using the chapter.md.tmpl template
func CreateChapterFile(chapterPath, title string) error {
	// Load the book metadata to get the author information
	bookMetadata := ReadBookMetadata()

	// Define the data to pass into the template
	data := struct {
		Title  string
		Date   string
		Author string
	}{
		Title:  title,
		Date:   time.Now().Format("2006-01-02"),
		Author: bookMetadata.Author, // Load author from book.yaml
	}

	// Define file creation path
	bookDir := filepath.Dir(chapterPath)
	subDir := "" // Since the chapterPath already includes the directory, we don't need a separate subDir

	// Use CreateFileFromTemplate to generate the chapter file
	err := templates.CreateFileFromTemplate(bookDir, subDir, filepath.Base(chapterPath), "chapter.md", data)
	if err != nil {
		return err
	}

	return nil
}

// CreateNoteFromTemplate creates a note using a specific template
func CreateNoteFromTemplate(notePath, tmplName, title string) error {
	// Load the book metadata to get the author information
	bookMetadata := ReadBookMetadata()

	// Define the data to pass into the template
	data := struct {
		Name   string
		Date   string
		Author string
	}{
		Name:   title,
		Date:   time.Now().Format("2006-01-02"),
		Author: bookMetadata.Author, // Load author from book.yaml
	}

	// Use CreateFileFromTemplate to generate the note file
	err := templates.CreateFileFromTemplate("", "", notePath, tmplName, data)
	if err != nil {
		return fmt.Errorf("error creating note '%s': %v", notePath, err)
	}

	return nil
}

// GetMicrosoftAccessToken fetches an access token using OAuth2
func GetMicrosoftAccessToken(clientID, clientSecret, tenantID string) (string, error) {
	tokenURL := fmt.Sprintf("https://login.microsoftonline.com/%s/oauth2/v2.0/token", tenantID)
	data := url.Values{}
	data.Set("client_id", clientID)
	data.Set("scope", "https://graph.microsoft.com/.default")
	data.Set("client_secret", clientSecret)
	data.Set("grant_type", "client_credentials")

	req, err := http.NewRequest("POST", tokenURL, strings.NewReader(data.Encode()))
	if err != nil {
		return "", err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	// Check for HTTP status code errors
	if resp.StatusCode != http.StatusOK {
		bodyBytes, _ := io.ReadAll(resp.Body)
		return "", fmt.Errorf("failed to get token: %s - %s", resp.Status, string(bodyBytes))
	}

	// Parse response body
	var result map[string]interface{}
	if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return "", fmt.Errorf("failed to decode response: %v", err)
	}

	// Extract access token
	if accessToken, ok := result["access_token"].(string); ok {
		return accessToken, nil
	}
	return "", fmt.Errorf("failed to retrieve access token")
}

// GetMicrosoftToDoListID retrieves the ID of a Microsoft To-Do list by name
func GetMicrosoftToDoListID(accessToken, listName string) (string, error) {
	url := "https://graph.microsoft.com/v1.0/me/todo/lists"

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}
	req.Header.Add("Authorization", "Bearer "+accessToken)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	var result map[string]interface{}
	json.NewDecoder(resp.Body).Decode(&result)

	// Search for the list by name
	for _, list := range result["value"].([]interface{}) {
		listData := list.(map[string]interface{})
		if listData["displayName"].(string) == listName {
			return listData["id"].(string), nil
		}
	}

	// If list not found, create it
	return CreateMicrosoftToDoList(accessToken, listName)
}

// CreateMicrosoftToDoList creates a new Microsoft To-Do list and returns its ID
func CreateMicrosoftToDoList(accessToken, listName string) (string, error) {
	url := "https://graph.microsoft.com/v1.0/me/todo/lists"

	listData := map[string]interface{}{
		"displayName": listName,
	}
	jsonData, err := json.Marshal(listData)
	if err != nil {
		return "", err
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonData))
	if err != nil {
		return "", err
	}
	req.Header.Add("Authorization", "Bearer "+accessToken)
	req.Header.Add("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	var result map[string]interface{}
	json.NewDecoder(resp.Body).Decode(&result)

	if id, ok := result["id"].(string); ok {
		return id, nil
	}
	return "", fmt.Errorf("failed to create list")
}

// AddTaskToMicrosoftToDoList adds a task to a specified Microsoft To-Do list
func AddTaskToMicrosoftToDoList(accessToken, listID, taskName string) error {
	url := fmt.Sprintf("https://graph.microsoft.com/v1.0/me/todo/lists/%s/tasks", listID)

	taskData := map[string]interface{}{
		"title": taskName,
	}
	jsonData, err := json.Marshal(taskData)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonData))
	if err != nil {
		return err
	}
	req.Header.Add("Authorization", "Bearer "+accessToken)
	req.Header.Add("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusCreated {
		return fmt.Errorf("failed to create task, status code: %d", resp.StatusCode)
	}

	return nil
}
