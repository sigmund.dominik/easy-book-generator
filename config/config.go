package config

import (
	"encoding/json"
	"fmt"
	"os"
	"os/user"
	"path/filepath"
)

type Configuration struct {
	Author             string `json:"author"`
	MicrosoftClientID  string `json:"microsoft_client_id"`
	MicrosoftClientSecret string `json:"microsoft_client_secret"`
	MicrosoftTenantID  string `json:"microsoft_tenant_id"`
	MicrosoftAccessToken string `json:"microsoft_access_token"`
}

var Config Configuration
var configPath string

// LoadConfig loads the configuration from the user's home directory
func LoadConfig() error {
	usr, err := user.Current()
	if err != nil {
		return fmt.Errorf("error getting current user: %v", err)
	}

	configPath = filepath.Join(usr.HomeDir, ".ebg-config")

	file, err := os.Open(configPath)
	if err != nil {
		if os.IsNotExist(err) {
			return nil // No config file yet, that's fine
		}
		return fmt.Errorf("error opening config file: %v", err)
	}
	defer file.Close()

	decoder := json.NewDecoder(file)
	if err := decoder.Decode(&Config); err != nil {
		return fmt.Errorf("error decoding config file: %v", err)
	}

	return nil
}

// SaveConfig saves the current configuration to the user's home directory
func SaveConfig() error {
	file, err := os.Create(configPath)
	if err != nil {
		return fmt.Errorf("error creating config file: %v", err)
	}
	defer file.Close()

	encoder := json.NewEncoder(file)
	if err := encoder.Encode(&Config); err != nil {
		return fmt.Errorf("error encoding config file: %v", err)
	}

	return nil
}

// SetAuthor sets the author's name and saves the configuration
func SetAuthor(author string) error {
	Config.Author = author
	return SaveConfig()
}

// GetAuthor retrieves the currently configured author
func GetAuthor() string {
	return Config.Author
}

// SetMicrosoftCredentials sets Microsoft API credentials and saves them
func SetMicrosoftCredentials(clientID, clientSecret, tenantID, accessToken string) error {
	Config.MicrosoftClientID = clientID
	Config.MicrosoftClientSecret = clientSecret
	Config.MicrosoftTenantID = tenantID
	Config.MicrosoftAccessToken = accessToken
	return SaveConfig()
}

// GetMicrosoftClientID retrieves the stored Microsoft Client ID
func GetMicrosoftClientID() string {
	return Config.MicrosoftClientID
}

// GetMicrosoftClientSecret retrieves the stored Microsoft Client Secret
func GetMicrosoftClientSecret() string {
	return Config.MicrosoftClientSecret
}

// GetMicrosoftTenantID retrieves the stored Microsoft Tenant ID
func GetMicrosoftTenantID() string {
	return Config.MicrosoftTenantID
}

// GetMicrosoftAccessToken retrieves the stored Microsoft Access Token
func GetMicrosoftAccessToken() string {
	return Config.MicrosoftAccessToken
}