package templates

import (
	"embed"
	"os"
	"path/filepath"
	"text/template"
	"fmt"
)

//go:embed *.tmpl
var templateFS embed.FS

// LoadTemplate loads a template from the embedded filesystem
func LoadTemplate(tmplName string) (*template.Template, error) {
	content, err := templateFS.ReadFile(tmplName + ".tmpl")
	if err != nil {
		return nil, fmt.Errorf("error reading template '%s': %v", tmplName, err)
	}
	return template.New(tmplName).Parse(string(content))
}

// CreateFileFromTemplate creates a file from a template
func CreateFileFromTemplate(bookDir, subDir, filename, tmplName string, data interface{}) error {
	tmpl, err := LoadTemplate(tmplName)
	if err != nil {
		return err
	}

	filePath := filepath.Join(bookDir, subDir, filename)
	file, err := os.Create(filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	if err := tmpl.Execute(file, data); err != nil {
		return err
	}

	return nil
}