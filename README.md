
# Easy Book Generator

Helps you to create a book from a collection of markdown files.

## Concepts

### States of Chapters

1. todo: The chapter needs to be created or has significant work remaining.
2. draft: The chapter has been started but is not yet complete.
3. review: The chapter is complete but needs to be reviewed.
4. done: The chapter is complete and no further work is needed.

```mermaid
stateDiagram-v2
    [*] --> Todo
    Todo --> Draft : Start
    Draft --> Review : Submit for Review
    Review --> Done : Approve
    Review --> Draft : Request Changes
    Done --> [*]
```

### Explanation

- **States**: `Todo`, `Draft`, `Review`, `Done`
- **Transitions**:
  - Start at `Todo`
  - Move to `Draft`
  - Submit for review (`Draft` -> `Review`)
  - Approve (`Review` -> `Done`)
  - Request changes (`Review` -> `Draft`)
  - Finish at `Done`

## Installation

### Install Dependencies

Install pandoc:

```bash
sudo apt-get install pandoc
```

Install imagemagick:

```bash
sudo apt-get install imagemagick
```

Install poppler-utils:

```bash
sudo apt-get install poppler-utils
```

Install git:

```bash
sudo apt-get install git
```

### Install ebg

Clone the repository and build the application:

```bash
git clone https://gitlab.com/your-repository/easy-book-generator.git
cd easy-book-generator
go build -o ebg
ln -s $PWD/ebg /usr/local/bin/ebg
chmod +x /usr/local/bin/ebg
```

## Installation and Usage via Docker

```bash
docker run --rm -v $(pwd):/workspace registry.gitlab.com/your-repository/easy-book-generator:latest [your-commands-here]
```

## Usage

```bash
ebg
```

### Create a new book

```bash
ebg init
```

### Add a new part

```bash
ebg add part "Part Name"
```

### Add a new chapter

```bash
ebg add chapter "Part Name/Chapter Name"
```

### Build the book

```bash
ebg build book pdf
```

### Build the expose

```bash
ebg build expose
```

### Notes and Note Management

When working on a fictional book, you can organize and create notes for different aspects of your story, such as characters, places, events, and more. The ebg tool allows you to manage these notes.

First initialize the notes on the creation of the book:

```bash
ebg init --add-notes
```

Then you can add notes for different categories:

```bash
ebg add note <name> <type>
```

Supported note types include:

- character: Information about characters in the story.
- place: Descriptions of important locations.
- event: Details about significant events.
- item: Information about key objects or artifacts.
- scene: Descriptions of important scenes.
- woldbuilding: Notes about the world and its rules.
- theme: Information about the themes of the story.

For example, to add a new character note:

```bash
ebg add note "John Doe" character
```
(You can also manually create a new markdown file in the `notes/characters` directory.)

Each type of note follows a specific structure to help organize information consistently.


### Analyze your Content

The `ebg analyze` command allows you to analyze the text statistics of your book, a specific part, or a single chapter. This analysis includes various readability scores and text metrics to help you understand and improve your content.

#### Usage

```bash
  ebg analyze [book|part|chapter] [part-slug|chapter-slug] [score]
```

- **book**: Analyze the entire book.
- **part**: Analyze a specific part of the book.
- **chapter**: Analyze a specific chapter of the book.
- **[score]**: (Optional) Show a specific score.

#### Available Scores

- **AverageLettersPerWord**: Indicates the average number of letters in each word. Ideal range is 4.0 to 5.0.
- **AverageSyllablesPerWord**: Indicates the average number of syllables in each word. Ideal range is 1.4 to 1.8.
- **AverageWordsPerSentence**: Indicates the average number of words in each sentence. Ideal range is 14 to 20.
- **FleschKincaidReadingEase**: Indicates how easy a text is to read. Higher scores indicate easier readability. Ideal range is 60 to 70.
- **FleschKincaidGradeLevel**: Indicates the U.S. school grade level required to understand the text. Ideal range is 7 to 8.
- **GunningFogScore**: Estimates the years of formal education needed to understand the text on first reading. Ideal range is 6 to 12.
- **ColemanLiauIndex**: Indicates the U.S. school grade level required to understand the text. Ideal range is 8 to 10.
- **SMOGIndex**: Estimates the years of education needed to understand the text. Ideal range is 8 to 10.
- **AutomatedReadabilityIndex**: Indicates the U.S. school grade level required to understand the text. Ideal range is 8 to 10.
- **DaleChallReadabilityScore**: Estimates the understandability of the text. Ideal range is 6 to 8.

#### Examples

1. **Analyze the Entire Book**

```bash
ebg analyze book
```

This command analyzes the entire book and provides detailed statistics for all scores.

2. **Analyze a Specific Part**

```bash
ebg analyze part introduction
```

This command analyzes the part of the book with the slug `introduction` and provides detailed statistics for all scores.

3. **Analyze a Specific Chapter**

```bash
ebg analyze chapter chapter-1
```

This command analyzes the chapter of the book with the slug `chapter-1` and provides detailed statistics for all scores.

4. **Show a Specific Score for the Entire Book**
  
```bash
ebg analyze book FleschKincaidReadingEase
```

This command analyzes the entire book but only shows the Flesch-Kincaid Reading Ease score.

Use these commands to gain valuable insights into the readability and complexity of your content, helping you to make informed decisions for improvements.
