package main

import (
	"fmt"
	"os"

	"easy-book-generator/cmd"
)

var version = "1.8.0"

func main() {
	// Set version for Cobra
	cmd.RootCmd.Version = version

	// Execute Cobra commands
	if err := cmd.RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
